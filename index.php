<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 03.03.2018
 * Time: 15:24
 */
include 'modules\game\controller\GameController.php';
include 'modules\game\helper\GameHelper.php';

use modules\game\controller\GameController;

class Application
{
    public function run() {
        $game = new GameController();
        $game->generateRandomMap();
        $game->renderMap();
    }
}

$app = new Application();
$app->run();