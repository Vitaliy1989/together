<?php

/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 03.03.2018
 * Time: 16:19
 */
namespace modules\game\helper;

class GameHelper
{
    public static $locations = [];
    public static $units = [];
    public static $permissions = [];

    //list of military units
    const ENGINERY = 'Enginery';
    const PEOPLE = 'People';
    const AIRCRAFT = 'Aircraft';

    const BASE = 'Base';

    //list of area units
    const MOUNTAINS = 'Mountains';
    const WATER = 'Water';
    const SWAMP = 'Swamp';
    const FLATLAND = 'Flatland';

    public static function getLocations() {
        $locations = [self::MOUNTAINS,self::WATER,self::SWAMP,self::FLATLAND];

        return $locations;
    }

    public static function getUnits() {
        $units = [self::ENGINERY,self::PEOPLE,self::AIRCRAFT];

        return $units;
    }

    public static function getPermissions() {
        $permissions[self::MOUNTAINS] = [self::PEOPLE,self::AIRCRAFT];
        $permissions[self::WATER] = [self::PEOPLE,self::AIRCRAFT];
        $permissions[self::SWAMP] = [self::ENGINERY,self::AIRCRAFT];
        $permissions[self::FLATLAND] = [self::ENGINERY,self::PEOPLE,self::AIRCRAFT];

        return $permissions;
    }
}