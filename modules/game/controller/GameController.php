<?php

/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 04.03.2018
 * Time: 17:37
 */

namespace modules\game\controller;

require 'modules\game\model\base\Unit.php';
require 'modules\game\model\Area.php';
require 'modules\game\model\Base.php';
require 'modules\game\model\Enginery.php';
require 'modules\game\model\People.php';
require 'modules\game\model\Aircraft.php';

use modules\model\Area;
use modules\model\Base;
use modules\model\Enginery;
use modules\model\People;
use modules\model\Aircraft;
use modules\game\helper\GameHelper;

class GameController
{
    const COUNT_BASES = 2;

    public $width = null;
    public $height = null;
    public $map = [];
    public $units = [];
    public $locations = [];
    public $permissions = [];

    public function __construct()
    {
        $this->units = GameHelper::getUnits();
        $this->locations = GameHelper::getLocations();
        $this->permissions = GameHelper::getPermissions();
    }

    private function getInstanceOfUnit($name)
    {
        $unit = null;

        switch($name):
            case(GameHelper::ENGINERY):
                $unit = new Enginery();
                break;
            case(GameHelper::PEOPLE):
                $unit = new People();
                break;
            case(GameHelper::AIRCRAFT):
                $unit = new Aircraft();
                break;
            endswitch;

        return $unit;
    }

    private function createRandomBases($count = null, $width = null, $height = null)
    {
        if(is_null($count))
            return;

        while ( $count != 0)
        {
            $rand_width = random_int(0, $width--);
            $rand_height = random_int(0, $height--);

            if(empty($this->map[$rand_width][$rand_height]))
            {
                $area  = new Area();
                $area->unit = new Base();
                $area->location = GameHelper::FLATLAND;

                $this->map[$rand_width][$rand_height] = $area;
                $count--;
            }
        }
    }

    private function createRandomArea(&$position)
    {
        $area = new Area();

        $rand_loc = array_rand($this->locations,1);
        $area->location = $this->locations[$rand_loc];;

        $units = $this->permissions[$this->locations[$rand_loc]];
        $rand_unit = array_rand($units,1);
        $area->unit = $this->getInstanceOfUnit($units[$rand_unit]);

        $position = $area;
    }

    public function generateRandomMap($width = 6, $height = 5) {

        $this->width = $width;
        $this->height = $height;

        $this->createRandomBases(self::COUNT_BASES, $width, $height);

        for ($i=0;$i<$width;$i++)
        {
            for ($j=0;$j<$height;$j++)
            {
                if(empty($this->map[$i][$j]))
                {
                    $this->createRandomArea($this->map[$i][$j]);
                }
            }
        }
    }

    public function getMap()
    {
        return $this->map;
    }

    public function renderMap() {

        if($this->map)
        {
            $output = '<table>';

            for ($i=0;$i<$this->width;$i++)
            {
                $output .= '<tr>';
                for ($j=0;$j<$this->height;$j++)
                {
                    $item = $this->map[$i][$j];

                    $output .= "<td>";
                    $output .= "<span>$item->location</span>";
                    $output .= "<br/>";
                    $output .= "<span>".$item->unit->getName()."</span>";
                    $output .= "</td>";
                }
                $output .= '</tr>';
            }
            $output .= '</table>';
        }

        echo $output;
    }
}