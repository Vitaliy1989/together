<?php

/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 04.03.2018
 * Time: 15:39
 */
namespace  modules\game\model\base;

interface UnitInterface
{
    public function getName();
    public function getLocations();
    public function getTargets();
    public function setName($name);
    public function setLocations($locations);
    public function setTargets($targets);
}