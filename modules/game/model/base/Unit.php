<?php

/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 03.03.2018
 * Time: 15:29
 */
namespace modules\game\model\base;

require 'modules\game\model\base\UnitInterface.php';

abstract class Unit implements UnitInterface
{
    protected $name = null;
    protected $locations = [];
    protected $targets = [];

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * @return array
     */
    public function getTargets()
    {
        return $this->targets;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param array $locations
     */
    public function setLocations($locations)
    {
        $this->locations = $locations;
    }

    /**
     * @param array $targets
     */
    public function setTargets($targets)
    {
        $this->targets = $targets;
    }
}