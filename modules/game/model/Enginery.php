<?php

/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 03.03.2018
 * Time: 15:45
 */
namespace modules\model;

use modules\game\model\base\Unit;
use modules\game\helper\GameHelper;

class Enginery extends Unit
{
    public function __construct()
    {
        $this->setName(GameHelper::ENGINERY);
        $this->setLocations([GameHelper::SWAMP,GameHelper::FLATLAND]);
        $this->setTargets([GameHelper::ENGINERY, GameHelper::PEOPLE]);
    }
}