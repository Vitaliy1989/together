<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 03.03.2018
 * Time: 16:59
 */

namespace modules\model;

use modules\game\model\base\Unit;
use modules\game\helper\GameHelper;

class Aircraft extends Unit
{
    public function __construct()
    {
        $this->setName(GameHelper::AIRCRAFT);
        $this->setLocations([GameHelper::MOUNTAINS, GameHelper::WATER, GameHelper::SWAMP, GameHelper::FLATLAND]);
        $this->setTargets([GameHelper::ENGINERY]);
    }
}